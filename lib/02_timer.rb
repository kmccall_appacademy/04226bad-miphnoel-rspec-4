# Functionality extends beyond spec parameters for usage convenience

class Timer
  attr_reader :seconds, :minutes, :hours

  def initialize
    @seconds = 0
    @minutes = 0
    @hours = 0
  end

  def seconds=(seconds)
    @seconds = seconds
    @minutes = seconds / 60
    @hours = seconds / 3600
  end

  def minutes=(minutes)
    @seconds = minutes * 60
    @minutes = minutes
    @hours = minutes / 60
  end

  def hours=(hours)
    @seconds = hours * 3600
    @minutes = hours * 60
    @hours = hours
  end

  def time_string
    time = [@hours, @minutes % 60, @seconds % 60]
    substrings = time.map { |digit| padded_string(digit) }
    substrings.join(':')
  end

  def padded_string(digit)
    string = digit.to_s
    string.length < 2 ? "0#{string}" : string
  end
end
