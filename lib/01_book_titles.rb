class Book

  EXCEPTIONS = %w[the a an and in of]

  attr_reader :title

  def title=(value)
    words = value.split

    words.map! do |word|
      EXCEPTIONS.include?(word) ? word : word.capitalize
    end

    words[0].capitalize!

    @title = words.join(' ')
  end
end
