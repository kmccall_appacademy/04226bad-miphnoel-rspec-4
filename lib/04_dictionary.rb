class Dictionary
  attr_reader :entries

  def initialize
    @entries = {}
  end

  def add(entry)
    entry.is_a?(String) ? @entries[entry] = nil : @entries.merge!(entry)
  end

  def keywords
    @entries.keys.sort
  end

  def include?(keyword)
    @entries.key?(keyword)
  end

  def find(search_term)
    @entries.select { |keyword| keyword.include?(search_term) }
  end

  def printable
    printable_entries = keywords.map do |keyword|
      %([#{keyword}] "#{@entries[keyword]}")
    end

    printable_entries.join("\n")
  end
end
