class Temperature

  def initialize(scale_temp_hash)
    @scale = scale_temp_hash.keys[0]
    @temperature = scale_temp_hash.values[0]
  end

  def self.from_celsius(temp)
    Temperature.new(:c => temp)
  end

  def self.from_fahrenheit(temp)
    Temperature.new(:f => temp)
  end

  def in_fahrenheit
    @scale == :f ? @temperature : ctof(@temperature)
  end

  def in_celsius
    @scale == :c ? @temperature : ftoc(@temperature)
  end

  def ftoc(temp)
    c_temp = (temp - 32).to_f * 5 / 9
    (c_temp % 1).zero? ? c_temp.floor : c_temp
  end

  def ctof(temp)
    f_temp = (temp * 9).to_f / 5 + 32
    (f_temp % 1).zero? ? f_temp.floor : f_temp
  end

end

class Celsius < Temperature
  def initialize(temp)
    @scale = :c
    @temperature = temp
  end
end

class Fahrenheit < Temperature
  def initialize(temp)
    @scale = :f
    @temperature = temp
  end
end
